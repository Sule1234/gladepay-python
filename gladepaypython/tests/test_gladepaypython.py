import os
from  unittest import TestCase
from gladepaypython import Gladepay

class TestGladepay(TestCase):

    def setUp(self):
        super(TestGladepay, self).setUp()
        self.test_merchant_id = os.getenv('_GLADEPAY_MERCHANT_ID', 'GP0000001')
        self.test_merchant_key = os.getenv('_GLADEPAY_MERCHANT_KEY', '123456789')
        self.gladepay = Gladepay(self.test_merchant_id, self.test_merchant_key)
        self.initialize_vals = {
            'action': 'initiate',
            'paymentType': 'card',
            'user': {
              'firstname': 'Chinaka',
              'lastname': 'Light',
              'email': 'test@gmail.com',
              'ip': '192.168.33.10',
              'fingerprint': 'cccvxbxbxb'
            },
            'card': {
              'card_no': '5438898014560229',
              'expiry_month': '09',
              'expiry_year': '19',
              'ccv': '789',
              'pin': '3310'
            },
            'amount': '10000',
            'country': 'NG',
            'currency': 'NGN'
          }

        self.charge_initialize = {
            'action': 'charge',
            'paymentType': 'account',
            'user': {
                'firstname': 'Chinaka',
                'lastname': 'Light',
                'email': 'test@gladepay.com',
                'ip': '192.168.33.10',
                'fingerprint': 'ddsdschhdghgshghdgshghcx'
            },
            'account': {
                'accountnumber': '0690000007',
                'bankcode': '044'
            },
            'amount': '10000'
          }

        self.token_initialize = {
            'action': 'charge',
            'paymentType': 'token',
            'token': '01fae68d805957cc24697c990961a130',
            'user': {
                'firstname': 'Chinaka',
                'lastname': 'Light',
                'email': 'test@gladepay.com',
                'ip': '192.168.33.10',
                'fingerprint': 'cccvxbxbxb'
            },
            'account': {
                'accountnumber': '0690000007',
                'bankcode': '044'
            },
            'amount': '10000'
          }


    def test_card_payment(self):
        response = self.gladepay.card_payment(self.initialize_vals['amount'], self.initialize_vals['country'], self.initialize_vals['currency'], self.initialize_vals['user'], self.initialize_vals['card'])
        self.assertNotEqual(response['status'], 400)
        assert response['status'] == 500 or response['status'] == 104 or response['status'] == 202


    def test_verify(self):
       pass

    def test_that_glade_instance_is_test_mode(self):
        gladepay = Gladepay(self.test_merchant_id, self.test_merchant_key)
        self.assertNotEqual(gladepay, None)
        self.assertEqual(gladepay._url('/'), 'https://demo.api.gladepay.com/')
        self.assertEqual(self.gladepay._url('/'), 'https://demo.api.gladepay.com/')

    def test_that_instance_is_in_live_mode(self):
        gladepay = Gladepay(self.test_merchant_id, self.test_merchant_key, True)
        self.assertNotEqual(gladepay, None)
        self.assertEqual(gladepay._url('/'), 'https://api.gladepay.com/')

    def test_that_glade_card_payment_initilized_properly(self):
        response = self.gladepay.card_payment(
            self.initialize_vals['user'],
            self.initialize_vals['card'],
            self.initialize_vals['amount'],
            self.initialize_vals['country'],
            self.initialize_vals['currency']
        )

        self.print_val('RESPONSE-GT-CARD-PAYMENT-INITIALIZED-PROPERLY: ')
        self.print_val(response)
        self.print_val("\n")
        assert response['status'] == 200 or response['status'] == 202 or response['status'] == 301 or response['status'] == 500

    def test_that_glade_validate_otp_code(self):
        response = self.gladepay.card_payment(
            self.initialize_vals['user'],
            self.initialize_vals['card'],
            self.initialize_vals['amount'],
            self.initialize_vals['country'],
            self.initialize_vals['currency']
        )
        self.print_val("BEFORE VALIDATE OTP")
        self.print_val(response)
        assert response['status'] == 200 or response['status'] == 202 or response['status'] == 500
        self.print_val("VALIDATE OTP USING TRANSACTION_REFERENCE " + response.get('txnRef', 'SAMPDUMP14'))
        response = self.gladepay.validate_otp(response.get('txnRef', 'DUMP12132'), '12345')
        self.print_val(response)
        assert response['status'] == 200 or response['status'] == 202 or response['status'] == 500 or response['status'] == 104


    def test_g_validation_code(self):
        response = self.gladepay.card_payment(
            self.initialize_vals['user'],
            self.initialize_vals['card'],
            self.initialize_vals['amount'],
            self.initialize_vals['country'],
            self.initialize_vals['currency']
        )

        self.print_val(response)
        assert response['status'] == 200 or response['status'] == 202 or response['status'] == 500

        response = self.gladepay.validate_otp(response.get('txnRef', 'SAMPDUMP12'), '12345')
        self.print_val(response)

        assert response['status'] == 200 or response['status'] == 202 or response['status'] == 500 or response['status'] == 104
        self.print_val("VERIFYING USING TRANSACTION_REFERENCE " + response.get('txnRef', 'SAMPDUMP13'))
        response = self.gladepay.verify_transaction(response.get('txnRef', 'SAMPDUMP13'))
        self.print_val(response)
        assert response['status'] == 200 or response['status'] == 202 or response['status'] == 104 or response['status'] == 500


    def test_banks(self):
        all_banks_response = self.gladepay.all_banks
        self.assertNotEqual(all_banks_response, None)
        supported_banks_response = self.gladepay.supported_banks_account_payment
        self.assertNotEqual(supported_banks_response, None)

    def test_banks_and_charge_with_token(self):
        all_banks_response = self.gladepay.all_banks()

        self.assertNotEqual(all_banks_response, None)
        self.print_val('ALL BANKS')
        self.print_val(all_banks_response)

        supported_banks_response = self.gladepay.supported_banks_account_payment
        self.assertNotEqual(supported_banks_response, None)
        self.print_val('ALL SUPPORTED BANKS')
        self.print_val(supported_banks_response)
        response = self.gladepay.charge_with_token(
          self.token_initialize['token'],
          self.token_initialize['amount'],
          self.token_initialize['user']
        )

        self.print_val('CHARGE WITH TOKEN RESPONSE')
        self.assertNotEqual(response, None)
        self.print_val(response)


    def test_account_payment(self):
        account_payment_response = self.gladepay.account_payment(self.charge_initialize['user'], self.charge_initialize['account'], self.initialize_vals['amount'])
        self.assertNotEqual(account_payment_response, None)
        self.print_val('Account Payment RESPONSE ')
        self.print_val(account_payment_response)
        assert account_payment_response['status'] == 200 or account_payment_response['status'] == 202 or account_payment_response['status'] == 103 or account_payment_response['status'] == 104
        card_details_response = self.gladepay.card_details('1234567898')
        self.assertNotEqual(card_details_response, None)
        self.print_val('Card Details')
        self.print_val(card_details_response)


    def test_card_charges(self):
        test_card_charges_response = self.gladepay.card_charges(self.initialize_vals['amount'], '543889')
        self.print_val('CARD CHARGES RESPONSE')
        self.print_val(test_card_charges_response)
        self.assertNotEqual(test_card_charges_response, None)

    def test_account_name_verification(self):
        test_account_name_verification_response = self.gladepay.verify_account_name(self.charge_initialize['account']['bankcode'], self.charge_initialize['account']['accountnumber'])
        self.print_val('ACCOUNT NAME VERIFICATION')
        self.print_val(test_account_name_verification_response)
        self.assertNotEqual(test_account_name_verification_response, None)

    def test_account_charges(self):
        self.print_val('NON-CARD OR ACCOUNT CHARGES BEGIN')
        test_account_charges_response = self.gladepay.account_charges(self.initialize_vals['amount'])
        self.print_val('NON-CARD OR ACCOUNT CHARGES RESPONSE')
        self.print_val(test_account_charges_response)
        assert (test_account_charges_response != None)
        # assert_kind_of Numeric, test_account_charges_response

    def test_transfer(self):
        test_money_transfer_response = self.gladepay.money_transfer(self.charge_initialize['amount'], self.charge_initialize['account']['bankcode'], self.charge_initialize['account']['accountnumber'], 'Mark Silas', 'Narration')
        self.assertNotEqual(test_money_transfer_response, None)
        self.print_val('MONEY TRANSFER RESPONSE')
        self.print_val(test_money_transfer_response)
        assert test_money_transfer_response['status'] == 200 or test_money_transfer_response['status'] == 202 or test_money_transfer_response['status'] == 301

        verify_money_transfer_response = self.gladepay.verify_money_transfer(test_money_transfer_response.get('txnRef', 'SAMPDUMP15'))
        self.assertNotEqual(verify_money_transfer_response, None)
        self.print_val('VERIFYING MONEY TRANSFER RESPONSE')
        self.print_val(verify_money_transfer_response)
        assert verify_money_transfer_response['status'] == 200 or verify_money_transfer_response['status'] == 202 or verify_money_transfer_response['status'] == 203

    def print_val(self, param):
        print(param)
        pass